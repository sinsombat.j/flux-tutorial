import Dispatcher from '../dispatcher/AppDispatcher'
import AppConstant from '../constants/AppConstant'

import EventEmitter from 'events'

let CHANGE_EVENT = 'change';

let _state = {}
let _comments = []

function addData(comment) {
    let id = (+new Date() + Math.floor(Math.random() * 999999)).toString(36);
    let newMessage = [{
        'ID': id,
        'title': comment
    }];
    _comments = newMessage.concat(_comments);
}

class Store extends EventEmitter{

    getState() {
        return _state;
    }

    getAll() {
        return _comments;
    }

    emitChange() {
        this.emit(CHANGE_EVENT);
    }
    addChangeListener(callback) {
        this.on(CHANGE_EVENT, callback);
    }
    removeChangeListener(callback) {
        this.removeListener(CHANGE_EVENT, callback);
    }

}

Dispatcher.register(function(action) {

    console.log(action)

    switch (action.actionType) {
        case AppConstant.DISCUSSION_CREATE:
            addData(action.comment);
            break;
        default:
            // no op
    }

    Store.emitChange();
})

export default new Store()
