import Dispatcher from '../dispatcher/AppDispatcher'
import AppConstant from '../constants/AppConstant'

export default class Action {
  addComment(comment){

    Dispatcher.dispatch({
        actionType: AppConstant.DISCUSSION_CREATE,
        comment: comment,
    })
  }
}
