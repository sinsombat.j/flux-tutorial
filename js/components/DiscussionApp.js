import React from 'react'
import Store from '../stores/store'
import Action from '../actions/AppAction'

import DiscussionForm from './DiscussionForm.js'
import DiscussionList from './DiscussionList.js'


export default class DiscussionApp extends React.Component{


  constructor(props) {
      super(props)
      this.state = Store.getState();
  }

  componentDidMount(){
        Store.addChangeListener(this._onChange);
  }

  componentWillUnmount(){
        Store.removeChangeListener(this._onChange);
  }
  _onChange(){
        this.setState(Store.getState());
  }

  render() {
      let comments = Store.getAll();
      return (
          <div>
              <DiscussionForm />
              <DiscussionList comments={comments}/>
          </div>
      );
  }
}
