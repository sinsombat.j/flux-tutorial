import React from 'react'
import Action from '../actions/AppAction'

export default class DiscussionForm extends React.Component{


  constructor(props){
    super(props)
    this.state = {message :''}
    this._onChange = this._onChange.bind(this)
    this._onSubmit = this._onSubmit.bind(this)
  }

  _onChange(event) {
      this.setState({
          message: event.target.value
      });
  }

  _onSubmit(event) {
      event.preventDefault();

      Action.addComment(this.state.message)

      this.setState({
          message: ''
      });
  }

  render(){
    return (
        <form >
            <input type="text"
              placeholder="Enter message here..."
              onChange={this._onChange}
              value={this.state.message}
            />
          <button onclick ={this._onSubmit}>Comment</button>
        </form>
    );
  }
}
