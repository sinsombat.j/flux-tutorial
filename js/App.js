import React,{Component} from 'react'
import ReactDom,{render} from 'react-dom'

import DiscussionApp from './components/DiscussionApp.js'

render(<DiscussionApp /> , document.getElementById('app'))
